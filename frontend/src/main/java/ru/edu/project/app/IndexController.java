package ru.edu.project.app;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Collection;

@Controller
public class IndexController {

    /**
     * Строковое представление роли студента.
     */
    public static final String ROLE_STUDENT_STR = "STUDENT";

    /**
     * Строковое представление роли преподавателя.
     */
    public static final String ROLE_TEACHER_STR = "TEACHER";

    /**
     * Строковое представление роли администратора.
     */
    public static final String ROLE_ADMINISTRATOR_STR = "ADMINISTRATOR";

    /**
     * Объект роли клиента.
     */
    public static final SimpleGrantedAuthority ROLE_STUDENT = new SimpleGrantedAuthority(ROLE_STUDENT_STR);

    /**
     * Объект роли менеджера.
     */
    public static final SimpleGrantedAuthority ROLE_TEACHER = new SimpleGrantedAuthority(ROLE_TEACHER_STR);

    /**
     * Объект роли администратора.
     */
    public static final SimpleGrantedAuthority ROLE_ADMINISTRATOR = new SimpleGrantedAuthority(ROLE_ADMINISTRATOR_STR);

    /**
     * Точка входа.
     *
     * @param authentication
     * @return string
     */
    @GetMapping("/")
    public String index(final Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated()) {
            return redirectByRole(authentication.getAuthorities());
        }
        return "index";
    }

    private String redirectByRole(final Collection<? extends GrantedAuthority> authorities) {
        if (authorities.contains(ROLE_STUDENT)) {
            return "redirect:/user/student/";
        }

        if (authorities.contains(ROLE_TEACHER)) {
            return "redirect:/user/teacher/";
        }

        if (authorities.contains(ROLE_ADMINISTRATOR)) {
            return "redirect:/user/administrator/";
        }
        return "index";
    }

}
