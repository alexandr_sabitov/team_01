package ru.edu.project.backend.da.jpa.entity.lesson;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "lesson_group")
public class LessonGroupEntity {

    /**
     * Так как у нас составной ключ, необходимо вынести его в отдельную встраиваемую сущность.
     */
    @EmbeddedId
    private LessonGroupId pk;

    /**
     * Связь один к одному с таблицей LESSON через поле id_lesson.
     */
    @OneToOne
    @JoinColumn(name = "id_lesson", referencedColumnName = "id", insertable = false, updatable = false)
    private LessonEntity lesson;

    /**
     * Встраиваемая сущность описывающая поля входящие в состав составного ключа таблицы.
     */
    @Embeddable
    @Getter
    @Setter
    public static class LessonGroupId implements Serializable {

        /**
         * Составная часть ключа id_lesson.
         */
        @Column(name = "id_lesson")
        private Long idLesson;

        /**
         * Составная часть ключа id_group.
         */
        @Column(name = "id_group")
        private Long idGroup;
    }

    /**
     * Билдер первичного ключа.
     *
     * @param idLesson
     * @param idGroup
     * @return pk
     */
    public static LessonGroupId pk(final long idLesson, final long idGroup) {
        LessonGroupId id = new LessonGroupId();
        id.setIdLesson(idLesson);
        id.setIdGroup(idGroup);

        return id;
    }
}
