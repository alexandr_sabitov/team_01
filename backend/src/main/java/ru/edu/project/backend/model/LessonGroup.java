package ru.edu.project.backend.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
@Builder
public class LessonGroup {
    /**
     * Составная часть ключа id_user.
     */
    private Long idLesson;

    /**
     * Составная часть ключа id_group.
     */
    private Long idGroup;
}
