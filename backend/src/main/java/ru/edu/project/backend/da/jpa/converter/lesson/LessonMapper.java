package ru.edu.project.backend.da.jpa.converter.lesson;

import org.mapstruct.Mapper;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    /**
     * Маппер LessonEntity -> Lesson.
     *
     * @param entity
     * @return group
     */
    Lesson map(LessonEntity entity);

    /**
     * Маппер Lesson -> LessonEntity.
     *
     * @param entity
     * @return group
     */
    LessonEntity map(Lesson entity);

    /**
     * Маппер List<LessonEntity> -> List<Lesson>.
     * @param ids
     * @return list Role
     */
    List<Lesson> map(Iterable<LessonEntity> ids);
}
