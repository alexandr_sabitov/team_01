package ru.edu.project.backend.da.jpa.converter.lesson;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonGroupEntity;
import ru.edu.project.backend.model.LessonGroup;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonGroupMapper {

    /**
     * Маппер LessonGroupEntity -> LessonGroup.
     *
     * @param entity
     * @return group
     */
    @Mapping(source = "pk.idLesson", target = "idLesson")
    @Mapping(source = "pk.idGroup", target = "idGroup")
    LessonGroup map(LessonGroupEntity entity);

    /**
     * Маппер List<LessonGroupEntity> -> List<LessonGroup>.
     * @param ids
     * @return list Group
     */
    @Mapping(source = "pk.idLesson", target = "idLesson")
    @Mapping(source = "pk.idGroup", target = "idGroup")
    List<LessonGroup> map(Iterable<LessonGroupEntity> ids);
}
