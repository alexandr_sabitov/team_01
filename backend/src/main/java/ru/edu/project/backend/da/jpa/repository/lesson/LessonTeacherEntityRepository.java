package ru.edu.project.backend.da.jpa.repository.lesson;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonTeacherEntity;

import java.util.List;

@Repository
public interface LessonTeacherEntityRepository  extends CrudRepository<LessonTeacherEntity, LessonTeacherEntity.LessonTeacherId> {

    /**
     * Поиск записей по полю составного ключа pk.idLesson.
     *
     * @param idLesson
     * @return list entity
     */
    List<LessonTeacherEntity> findAllByPkIdLesson(long idLesson);

    /**
     * Поиск записей по полю составного ключа pk.idTeacher.
     *
     * @param idTeacher
     * @return list entity
     */
    List<LessonTeacherEntity> findAllByPkIdTeacher(long idTeacher);
}
