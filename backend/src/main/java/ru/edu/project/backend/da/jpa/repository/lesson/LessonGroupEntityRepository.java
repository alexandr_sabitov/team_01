package ru.edu.project.backend.da.jpa.repository.lesson;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.project.backend.da.jpa.entity.lesson.LessonGroupEntity;

import java.util.List;

@Repository
public interface LessonGroupEntityRepository extends CrudRepository<LessonGroupEntity, LessonGroupEntity.LessonGroupId> {

    /**
     * Поиск записей по полю составного ключа pk.idLesson.
     *
     * @param idLesson
     * @return list entity
     */
    List<LessonGroupEntity> findAllByPkIdLesson(long idLesson);

    /**
     * Поиск записей по полю составного ключа pk.idGroup.
     *
     * @param idGroup
     * @return list entity
     */
    List<LessonGroupEntity> findAllByPkIdGroup(long idGroup);
}
