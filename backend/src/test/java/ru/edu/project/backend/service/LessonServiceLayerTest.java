package ru.edu.project.backend.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonForm;
import ru.edu.project.backend.api.users.User;
import ru.edu.project.backend.da.LessonDALayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class LessonServiceLayerTest {

    @InjectMocks
    private LessonServiceLayer lessonServiceLayer;

    @Mock
    private LessonDALayer daLayer;

    @Mock
    private Lesson lesson;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getLesson() {
        when(daLayer.getLesson(1L)).thenReturn(lesson);

        assertEquals(lesson, lessonServiceLayer.getLesson(1L));
        verify(daLayer).getLesson(1L);
    }

    @Test
    void getLessonsByGroupId() {
        List<Lesson> lessonList = new ArrayList<>();

        when(daLayer.getLessonsByGroupId(1L)).thenReturn(lessonList);

        assertEquals(lessonList, lessonServiceLayer.getLessonsByGroupId(1L));
        verify(daLayer).getLessonsByGroupId(1L);
    }

    @Test
    void getLessonsByUserId() {
        List<Lesson> lessonList = new ArrayList<>();

        when(daLayer.getLessonsByUserId(1L)).thenReturn(lessonList);

        assertEquals(lessonList, lessonServiceLayer.getLessonsByUserId(1L));
        verify(daLayer).getLessonsByUserId(1L);
    }

    @Test
    void getLessonsInfoByIds() {
        List<Lesson> lessonList = new ArrayList<>();
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(daLayer.getLessonsInfoByIds(longs)).thenReturn(lessonList);

        assertEquals(lessonList, lessonServiceLayer.getLessonsInfoByIds(longs));
        verify(daLayer).getLessonsInfoByIds(longs);
    }

    @Test
    void getTeachersIdByLessonId() {
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(daLayer.getTeachersIdByLessonId(1L)).thenReturn(longs);

        assertEquals(longs, lessonServiceLayer.getTeachersIdByLessonId(1L));
        verify(daLayer).getTeachersIdByLessonId(1L);
    }

    @Test
    void createLesson() {
        LessonForm lessonForm = mock(LessonForm.class);
        when(daLayer.createLesson(any(Lesson.class))).thenAnswer(invocationOnMock -> {
            Lesson lessonInfo = invocationOnMock.getArgument(0, Lesson.class);
            return lesson;
        });
        lessonServiceLayer.createLesson(lessonForm);
    }

    @Test
    void getGroupsIdsByLessonId() {
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(daLayer.getGroupsIdsByLessonId(1L)).thenReturn(longs);

        assertEquals(longs, lessonServiceLayer.getGroupsIdsByLessonId(1L));
        verify(daLayer).getGroupsIdsByLessonId(1L);
    }
}