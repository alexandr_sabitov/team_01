package ru.edu.project.backend.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import ru.edu.project.backend.api.lesson.Lesson;
import ru.edu.project.backend.api.lesson.LessonForm;
import ru.edu.project.backend.service.LessonServiceLayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

class LessonControllerTest {

    @Mock
    private LessonServiceLayer delegate;

    @InjectMocks
    private LessonController lessonController;

    @Mock
    private Lesson lesson;

    @BeforeEach
    public void setUp() {
        openMocks(this);
    }

    @Test
    void getLesson() {
        when(delegate.getLesson(1L)).thenReturn(lesson);

        assertEquals(lesson, lessonController.getLesson(1L));
        verify(delegate).getLesson(1L);
    }

    @Test
    void getLessonsByGroupId() {
        List<Lesson> lessonList = new ArrayList<>();

        when(delegate.getLessonsByGroupId(1L)).thenReturn(lessonList);

        assertEquals(lessonList, lessonController.getLessonsByGroupId(1L));
        verify(delegate).getLessonsByGroupId(1L);
    }

    @Test
    void getLessonsByUserId() {
        List<Lesson> lessonList = new ArrayList<>();

        when(delegate.getLessonsByUserId(1L)).thenReturn(lessonList);

        assertEquals(lessonList, lessonController.getLessonsByUserId(1L));
        verify(delegate).getLessonsByUserId(1L);
    }

    @Test
    void getLessonsInfoByIds() {
        List<Lesson> lessonList = new ArrayList<>();
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(delegate.getLessonsInfoByIds(longs)).thenReturn(lessonList);

        assertEquals(lessonList, lessonController.getLessonsInfoByIds(longs));
        verify(delegate).getLessonsInfoByIds(longs);
    }

    @Test
    void getTeachersIdByLessonId() {
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(delegate.getTeachersIdByLessonId(1L)).thenReturn(longs);

        assertEquals(longs, lessonController.getTeachersIdByLessonId(1L));
        verify(delegate).getTeachersIdByLessonId(1L);
    }

    @Test
    void createLesson() {
        LessonForm lessonForm = mock(LessonForm.class);

        when(delegate.createLesson(lessonForm)).thenReturn(lesson);

        assertEquals(lesson, lessonController.createLesson(lessonForm));
        verify(delegate).createLesson(lessonForm);
    }

    @Test
    void getGroupsIdsByLessonId() {
        List<Long> longs = new ArrayList<>();
        longs.add(1L);

        when(delegate.getGroupsIdsByLessonId(1L)).thenReturn(longs);

        assertEquals(longs, lessonController.getGroupsIdsByLessonId(1L));
        verify(delegate).getGroupsIdsByLessonId(1L);
    }
}