package ru.edu.project.backend.api.lesson;

import ru.edu.project.backend.api.common.AcceptorArgument;

import java.util.List;

public interface LessonService {

    /**
     * Получение информации о занятии.
     *
     * @param id
     * @return Lesson
     */
    Lesson getLesson(long id);

    /**
     * Получение всех занятий по группе.
     * @param id
     * @return информация о группе
     */
    List<Lesson> getLessonsByGroupId(long id);

    /**
     *  Получение занятий по id пользователя.
     *
     * @param id
     * @return список
     */
    List<Lesson> getLessonsByUserId(long id);

    /**
     * Получение информации по занятиям по списку занятий.
     * @param ids
     * @return список
     */
    List<Lesson> getLessonsInfoByIds(List<Long> ids);

    /**
     * Получение id учителей по id занятия.
     *
     * @param id
     * @return список
     */
    List<Long> getTeachersIdByLessonId(long id);

    /**
     * Создание нового занятия.
     *
     * @param lessonForm
     * @return запись
     */
    @AcceptorArgument
    Lesson createLesson(LessonForm lessonForm);

    /**
     * Получение списка групп, которые связаны с уроком.
     *
     * @param id
     * @return list
     */
    List<Long> getGroupsIdsByLessonId(long id);
}
