package ru.edu.project.backend.api.lesson;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@Jacksonized
public class Lesson {

    /**
     * id занятия.
     */
    private Long id;

    /**
     * Номер классной комнаты.
     */
    private int classRoom;

    /**
     * Время начала занятий.
     */
    private Timestamp startTime;

    /**
     * Время конца занятий.
     */
    private Timestamp endTime;

}
