package ru.edu.project.backend.api.lesson;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Builder
@Jacksonized
public class LessonForm {

    /**
     * Номер классной комнаты.
     */
    private int classRoom;

    /**
     * Время начала занятий.
     */
    private Timestamp startTime;

    /**
     * Время конца занятий.
     */
    private Timestamp endTime;

    /**
     * id преподавателя.
     */
    private Long idTeacher;

    /**
     * Список групп, к которым добавляются занятия по ids.
     */
    private List<Long> idGroups;

}
